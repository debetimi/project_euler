

##Returns the difference between the sum of squares and square of sums for
# a range of number

def sum_square_difference(limit):
    values = range(1, limit+1)
    return sum(values)**2 - sum(map(lambda x: x**2, values))
