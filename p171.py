from math import sqrt, ceil, floor
from itertools import permutations

def generate_perfect_squares(digits):
    squares = []
    max_square = sqrt(digits * (9 ** 2));
    for i in range(1, int(max_square) + 1):
        squares.append(i ** 2)

    return squares

def create_list_of_numbers(value, master, remaining_nums, current, digits_left, max_digits, store_digits):

    if digits_left == 0:
        if value == 0:
            seen = set()
            for k in permutations(current, max_digits):
                if not k in seen:
                    seen.add(k)
                    if store_digits:
                        master.add(''.join(k)) 
                    else:
                        master[0] += 1
        return

    for i in range(len(remaining_nums)):
        option = remaining_nums[i]

        for digits_taken in range(1, digits_left + 1):
            sum_of_squares = digits_taken * ( option ** 2 )
            next_value = value - sum_of_squares 

            #check the out of bounds case
            #we want to break because surely increasing the number of digits
            #will be too much
            if next_value < 0:
                break 

            #check not enough juice left case
            #here we want to break 
            max_value = -1
            for num in remaining_nums:
                if num != option and num > max_value:
                    max_value = num

            if next_value > ( digits_left - 1 ) * ( max_value ** 2):
                break 

            #notice that we take the remain part of the list, we don't splice. thus we never have to deal with duplicates
            create_list_of_numbers(next_value, master, remaining_nums[i+1:],
                current + str(option)*digits_taken, digits_left - digits_taken, max_digits, store_digits)

def get_set_for_digits(digits, fn):
    master = set() 
    for square in generate_perfect_squares(19):
        remaining_nums = range(10)
        current = {}
        for i in range(10):
            current[i] = 0
        create_list_of_numbers(int(fn(square/2)), master, remaining_nums, '', digits, digits)
    return master

def main(n):
    #A plus b will be 19 digit letters that sum to w/e
    total = 0
    for square in generate_perfect_squares(n):
        #initialize the numbers you haven't used
        print 'square ', square
        remaining_nums = range(10)
        a = [0] 

        #get the set of 10 digit numbers whose sum square values is equal to the ceiling of the square/2
        desired_sum = int(ceil(square/2.0))
        if not square == 1:
            create_list_of_numbers(desired_sum, a, remaining_nums, '', 10, 10, False)

        #reinitialize your range and do the same thing for 9 digit numbers, except using the floor
        remaining_nums = range(10)
        b = set()

        if square == 1:
            desired_sum = int(ceil(square/2.0))
        else:
            desired_sum = int(floor(square/2.0))

        create_list_of_numbers(desired_sum, b, remaining_nums, '', 9, 9, True)

        partial = 0
        if not square == 1:
            partial += a[0]*sum(map(int, b))
        else:
            #special case only for when square equals to 1
            partial += sum(map(int, b))
        total += partial

    return total

