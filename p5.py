from collections import Counter
import itertools

# Helper function to prime factorize digit
# borrowed from prob3
def prime_factorization(number):

    factorized = []
    seed = 1
    while number != 1:
        seed += 1
        while not number % seed:
            factorized.append(seed)
            number = number / seed

    return Counter(factorized)



# finds the least common multiple of a range of values
# using the prime factorization method
def least_common_multiple(values):

    # get the prime factorizations of each value
    prime_factors = map(prime_factorization, values)

    # get the unique keys from the set
    keys = set(itertools.chain.from_iterable([counter.keys() for counter in prime_factors]))

    # initialize least common multiple
    lcm = 1

    # Go through the list and multiple the lcm by the highest power of each unique prime number
    # This can be described in a Venn diagram
    for key in keys:
        lcm *= pow(key, max([counter[key] for counter in prime_factors]))
    return lcm

# returns the smallest number divisible by every number
# up to and including the number provided
def divisible_up_to(number):

    forward = range(2, number+1)
    reverse = forward[::-1]

    for x in reverse:
        # remove factors of larger numbers to get to
        # core set of numbers we need to consider
        # this is not necesary but is good for optimization purposes
        if not x in forward:
            continue

        forward = [y for y in forward if x % y or x == y]

    # Find the least common multiple of these numbers
    return least_common_multiple(reverse)















