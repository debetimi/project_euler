import re
from operator import mul


#Find the greatest product of five consecutive digits in the 1000-digit number.

number = int("""
73167176531330624919225119674426574742355349194934
96983520312774506326239578318016984801869478851843
85861560789112949495459501737958331952853208805511
12540698747158523863050715693290963295227443043557
66896648950445244523161731856403098711121722383113
62229893423380308135336276614282806444486645238749
30358907296290491560440772390713810515859307960866
70172427121883998797908792274921901699720888093776
65727333001053367881220235421809751254540594752243
52584907711670556013604839586446706324415722155397
53697817977846174064955149290862569321978468622482
83972241375657056057490261407972968652414535100474
82166370484403199890008895243450658541227588666881
16427171479924442928230863465674813919123162824586
17866458359124566529476545682848912883142607690042
24219022671055626321111109370544217506941658960408
07198403850962455444362981230987879927244284909188
84580156166097919133875499200524063689912560717606
05886116467109405077541002256983155200055935729725
71636269561882670428252483600823257530420752963450 """.replace('\n', ""))


# remove the numbers padding 0's
def remove_zero_padding(index, test_string, n, maximum):
    if index == 0 or index == maximum:
        if len(test_string) >= n + 1:
            if index == 1:
                return test_string + '0'
            else:
                return '0' + test_string
                #return test_string[1:]
        else:
            return None
    else:
        return '0' + test_string + '0' if len(test_string) >= 2 else None
        #return test_string[1: len(test_string)-1] if len(test_string) >= n + 2 else None


def largest_product_n_digits(number, n=5):

    if len(number) == n:
        return reduce(mul, map(int, list(number)))

    max_prod = 1
    for x in range(0, len(number)-n):
        prod = reduce(mul, map(int, list(number[x: x+n])))
        if prod > max_prod:
            max_prod = prod

    #if max_prod == 1:
        #print number
    return max_prod


def largest_prod_in_series(number, n=5):
    # Split on 0's since product will equal 0
    candidates = re.split('0+', number)

    # strip the numbers that are 1 away from a zero
    candidates = [remove_zero_padding(index, num, 5, len(candidates)-1) for index, num in enumerate(candidates)]
    #print candidates
    candidates = set(candidates)
    candidates.remove(None)

    # remove blank entries
    #candidates = [number]
    #print map(largest_product_n_digits, candidates)
    return max(map(largest_product_n_digits, candidates))
