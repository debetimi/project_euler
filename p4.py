

# find the largest palindrone in range [1, 10^base)
# Problem 4
def largest_palindrone_number(base):

    allowable_factors = range(1, pow(10, base))

    largest_palindrone = 1

    for x in allowable_factors:

        #multiple this number with all other numbers in the allowable range
        products = map(lambda y: y*x, allowable_factors)

        # Get back palindrones by casting as string and checking foward equals reverse
        palindrones = [x for x in products if str(x) == str(x)[::-1]]

        local_max = max(palindrones) if palindrones else 0

        #Update local max is larger
        largest_palindrone = local_max if local_max > largest_palindrone\
            else largest_palindrone

    return largest_palindrone
