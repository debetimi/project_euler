from collections import deque

def too_easy(string):
    x = string.split(" ")
    reverse(x)
    return " ".join(x)

def reverse(list_):
    a = deque()
    for x in list_:
        a.append(x)
    for i,x in enumerate(list_):
        list_[i] = a.pop()
    return list_

def fib(n):
    return additive_sequence(n, 0, 1)

def additive_sequence(n, t0, t1):
    if n == 0:
        return t0
    return additive_sequence(n-1, t1, t0+t1)

def fibi(n):
    a = 0
    b = 1
    if n == 0:
        return a 
    if n == 1:
        return b

    for i in range(n):
        c = a + b
        a = b
        b = c
    return b