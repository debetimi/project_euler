import time
import copy
import random


def merge(left, right):
    new = []
    lindex = 0
    rindex = 0
    while lindex < len(left) and rindex < len(right):
        if left[lindex] > right[rindex]:
            new.append(left[lindex])
            lindex += 1
        else:
            new.append(right[rindex])
            rindex += 1
    if lindex < len(left):
        new.extend(left[lindex:])
    elif rindex < len(right):
        new.extend(right[rindex:])
    return new


def mergesort(array):
    if len(array) <= 1:
        return array
    else:
        left, right = array[:len(array)/2], array[len(array)/2:]
        return merge(mergesort(left), mergesort(right))


def test():

    times = []

    for i in range(10):
        start = time.time()
        y = [random.randint(0, 10000000) for r in xrange(1000000)]
        stop = time.time()
        print 'time to generate random nums', stop-start
        start = time.time()
        mergesort(y)
        stop = time.time()
        times.append(stop-start)
    print 'avg over 10 test: ', sum(times)/len(times)
