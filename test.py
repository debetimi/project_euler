import time
from tianlu import pwnTimi
from p8 import *
from random import randint


tianlu_times = []
timi_times = []

for x in range(400):
    seed = str(randint(10**9999,10**10000))
    start = time.clock()
    tlu = pwnTimi(seed)
    stop = time.clock()
    tianlu_times.append(stop-start)

    start = time.clock()
    timi = largest_prod_in_series(seed)
    stop = time.clock()
    timi_times.append(stop-start)

    assert(tlu == timi)

print sum(tianlu_times)/len(tianlu_times)
print sum(timi_times)/len(timi_times)
