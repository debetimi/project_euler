from operator import mul
# Problem 172 - Investigating numbers with few repeating digits
# How many 18-digit numbers n (without leading zeros) are there such that no digit occurs more than three times in n?
# _ _ _

# Thoughts
# Looks like a combination and permutation problem
# How to remove leading 0 cases...
# might want to work backwards
MAX_REP = 3


## Going to use recursion so memoize results
def memoize(func):
    cache = {}

    def wrap(*args):
        if args not in cache:
            cache[args] = func(*args)
        return cache[args]
    return wrap


# Define Permutation
# assuming valid ranges
@memoize
def nPk(n, k):
    if k == 0:
        return 1
    return reduce(mul, range(n-k+1, n+1))


# Define Combination:
# assuming valid ranges
@memoize
def nCk(n, k):
    return nPk(n, k) / nPk(k, k)


def num_pos(length, max_repeat):
    start = pow(10, length)
    max_repeat += 1
    for x in range(max_repeat, length + 1):
        seed = 10
        n = length
        k = x
        while True:
            multiple = nCk(n, x)
            if multiple != -1:
                start -= seed * multiple
            k = n - k




    
    return start - 1