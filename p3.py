

# Return the largest prime factor of a number
def largest_prime_factor(number):

    seed = 1
    while number != 1:
        seed += 1
        while not number % seed:
            number = number / seed

    return seed
