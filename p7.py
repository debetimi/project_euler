

# Returns the nth prime number
# not very efficient
def nth_prime_number(n):

    primes = [2]
    seed = 2
    while len(primes) < n:
        if False not in [seed % prime for prime in primes]:
            primes.append(seed)
        if seed == 2:
            seed += 1
        else:
            seed += 2

    return primes[-1]
