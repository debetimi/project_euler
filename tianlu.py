
def pwnTimi(number):
    biglist = [1]*len(number)
 
    # highest = 0
    for idx, digit_str in enumerate(number):
        for i in range(5):
            if (idx-i)<0: break
 
            biglist[idx-i]=biglist[idx-i]*int(digit_str)
            # if biglist[idx-i] > highest: highest = biglist[idx-i]
 
    biglist.sort(reverse=True)
 
    highest = biglist[0]
    return highest
