#!/usr/bin/python

seq = [1, 2]

while seq[-1] < 4e6:
    seq.append(seq[-1]+seq[-2])

return sum(x for x in seq if x % 2 == 0)
