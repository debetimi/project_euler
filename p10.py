from math import sqrt


def prime_number_less_than_n(n):
    prime_cache = []

    for x in range(2, n+1):
        if is_prime_number_n(x, prime_cache):
            prime_cache.append(x)

    return prime_cache


def is_prime_number_n(n, prime_cache):

    if not n % 2 and n != 2:
        return False

    if n == 2:
        return True

    check_lim = sqrt(n)
    for prime in prime_cache:
        if prime > check_lim:
            return True
        if not n % prime:
            return False





def sum_of_primes_less_than_n(n):
    prime_cache = []
    return sum(prime_number_less_than_n(n))
